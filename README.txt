This drupal module is an access point for applying for and receiving an interfacing relationship with the outside service of The People's Email Network, a proxy server submission engine for instantly sending your personal message directly to all your members of Congress (based on your address) with one click.  By using the system of The People's Email Network, you can have your own custom issue action form linking to the full power of its resource for free.

More on The People's Email Network here: http://www.usalone.com/

To use this module you must agree to the terms and conditions of connecting to The People's Email Network service included on the create question page of the module as well as those listed here: http://www.usalone.com/wtv_terms.htm

Included in this directory is the mysql file required for installation and the module itself.

Send comments to welch@advomatic.com

Originally developed for democrats.com by Aaron Welch (crunchywelch) at Advomatic LLC
